<?php

namespace App\Project;

use App\Entity\Project;
use App\Repository\TaskRepository;

class PercentageCounting
{
    private $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function countPercentageForProject(Project $project): int
    {
        $allTasksCount = $this->countAllTasksByProject($project) ?: 1;
        $finishedTasksCount = $this->countTasksFinishedForProject($project);
        $percentage = ($finishedTasksCount/$allTasksCount) * 100;

        return number_format($percentage, 2);
    }

    public function countAllTasksByProject(Project $project): int
    {
        return $this->taskRepository->getQueryBuilderForTasksPerProject($project)
            ->select('COUNT(t.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countTasksFinishedForProject(Project $project): int
    {
        return $this->taskRepository->getQueryBuilderForTasksPerProject($project)
            ->select('COUNT(t.id)')
            ->innerJoin('t.stage', 'ts')
            ->andWhere('ts.isClosingStage = :isClosingStage')
            ->setParameter('isClosingStage', true)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
