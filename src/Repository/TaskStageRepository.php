<?php

namespace App\Repository;

use App\Entity\TaskStage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TaskStage|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskStage|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskStage[]    findAll()
 * @method TaskStage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskStageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskStage::class);
    }

    // /**
    //  * @return TaskStage[] Returns an array of TaskStage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaskStage
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
