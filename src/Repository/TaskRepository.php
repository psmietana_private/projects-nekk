<?php

namespace App\Repository;

use App\Entity\Project;
use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function getQueryBuilderForTasksPerProject(Project $project): QueryBuilder
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.project', 'p', 'WITH', 'p.id = :pid')
            ->setParameter('pid', $project->getId());
    }

    public function getTasksOlderThanMonthForProject(Project $project): array
    {
        return  $this->getQueryBuilderForTasksPerProject($project)
            ->andWhere('t.createdAt < :monthAgo')
            ->setParameter('monthAgo', (new \DateTime())->modify('-1 month')->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getResult();
    }
}
