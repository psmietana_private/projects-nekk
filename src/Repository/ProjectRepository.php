<?php

namespace App\Repository;

use App\Entity\Project;
use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function getProjectsWithHighestPriorityTasks(): array
    {
        return $this->createQueryBuilder('p')
            ->innerJoin(Task::class, 't', 'WITH', 't.project = p.id')
            ->andWhere('t.priority = :maxPriority')
            ->setParameter('maxPriority', Task::MAX_PRIORITY)
            ->getQuery()
            ->getResult();
    }
}
