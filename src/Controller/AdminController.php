<?php

namespace App\Controller;

use App\Entity\Project;
use App\Project\PercentageCounting;
use App\Repository\ProjectRepository;
use App\Repository\TaskRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Pagerfanta\PagerfantaInterface;

class AdminController extends EasyAdminController
{
    private $projectRepository;
    private $taskRepository;

    public function __construct(ProjectRepository $projectRepository, TaskRepository $taskRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->taskRepository = $taskRepository;
    }

    public function projectTasksAction()
    {
        $this->entity = $this->get('easyadmin.config.manager')->getEntityConfig('Task');
        unset($this->entity['list']['fields']['id']);
        $this->entity['list']['fields']['stage']['sortable'] = false;

        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $paginator = $this->findProjectTasksPaginated(
            $this->projectRepository->find($this->request->query->get('id')),
            $this->entity['list']['max_results']
        );

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['list', $this->entity['templates']['list'], $parameters]);
    }

    private function findProjectTasksPaginated(Project $project, $maxPerPage = 15): PagerfantaInterface
    {
        $queryBuilder = $this->taskRepository->getQueryBuilderForTasksPerProject($project)
            ->innerJoin('t.stage', 'ts')
            ->addOrderBy('ts.isClosingStage', 'ASC')
            ->addOrderBy('ts.priority', 'ASC');

        $this->filterQueryBuilder($queryBuilder);

        $this->dispatch(EasyAdminEvents::POST_LIST_QUERY_BUILDER, [
            'query_builder' => $queryBuilder,
            'sort_field' => 'ts.isClosingStage',
            'sort_direction' => 'ASC',
        ]);

        return $this->get('easyadmin.paginator')->createOrmPaginator($queryBuilder, 1, $maxPerPage);
    }
}
