<?php

namespace App\Controller;

use App\Project\PercentageCounting;
use App\Repository\ProjectRepository;
use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class TestController extends AbstractController
{
    private $projectRepository;
    private $taskRepository;
    private $percentageCounting;

    public function __construct(
        ProjectRepository $projectRepository,
        TaskRepository $taskRepository,
        PercentageCounting $percentageCounting
    ) {
        $this->projectRepository = $projectRepository;
        $this->taskRepository = $taskRepository;
        $this->percentageCounting = $percentageCounting;
    }

    public function testAction()
    {
        $test1 = $this->projectRepository->getProjectsWithHighestPriorityTasks();
        var_dump($test1);

        $projects = $this->projectRepository->findAll();
        shuffle($projects);
        $project = $projects[0];
        $percentage = $this->percentageCounting->countPercentageForProject($project);
        if (50 < $percentage) {
            $test2 = $this->taskRepository->getTasksOlderThanMonthForProject($project);
            var_dump($test2);
        }

        return new Response;
    }
}
