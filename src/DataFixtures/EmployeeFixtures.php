<?php

namespace App\DataFixtures;

use App\Entity\Employee;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class EmployeeFixtures extends Fixture
{
    private static $firstNames = ['Adam', 'Jan', 'Tomek', 'John', 'testoweimię'];
    private static $lastNames = ['Kowalski', 'Nowak', 'Bilski', 'Smith', 'testowenazwisko'];

    public function load(ObjectManager $manager)
    {
        $total = rand(5, 10);
        for ($i = 1; $i <= $total; $i++) {
            shuffle(self::$firstNames);
            shuffle(self::$lastNames);
            $firstName = self::$firstNames[0];
            $lastName = self::$lastNames[0];

            $employee = new Employee();
            $employee->setFirstName($firstName);
            $employee->setLastName($lastName);
            $employee->setEmail(preg_replace('/[^a-zA-Z0-9_\-\s]/', '', $firstName . $lastName) . '@test.com');

            $manager->persist($employee);
        }
        $manager->flush();
    }
}
