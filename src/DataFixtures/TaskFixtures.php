<?php

namespace App\DataFixtures;

use App\Entity\Employee;
use App\Entity\Project;
use App\Entity\Task;
use App\Entity\TaskStage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TaskFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $total = rand(20, 30);
        $projects = $manager->getRepository(Project::class)->findAll();
        $stages = $manager->getRepository(TaskStage::class)->findAll();
        $employees = $manager->getRepository(Employee::class)->findAll() + [null];

        for ($i = 1; $i <= $total; $i++) {
            shuffle($projects);
            shuffle($stages);
            shuffle($employees);
            $priority = rand(1, Task::MAX_PRIORITY);

            $task = new Task();
            $number = $i + 1;
            $task->setName('Task testowy ' . $number);
            $task->setPriority($priority);
            $task->setProject($projects[0]);
            $task->setStage($stages[0]);
            $task->setEmployee($employees[0]);

            $manager->persist($task);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            EmployeeFixtures::class,
            TaskStageFixtures::class,
            ProjectFixtures::class,
        ];
    }
}
