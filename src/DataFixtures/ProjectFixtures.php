<?php

namespace App\DataFixtures;

use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProjectFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $total = rand(4, 7);
        for ($i = 1; $i <= $total; $i++) {
            $project = new Project();
            $number = $i + 1;
            $project->setName('Project ' . $number);
            $project->setClientName('Client ' . $number);

            $manager->persist($project);
        }
        $manager->flush();
    }
}
