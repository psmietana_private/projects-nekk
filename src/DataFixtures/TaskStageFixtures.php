<?php

namespace App\DataFixtures;

use App\Entity\TaskStage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TaskStageFixtures extends Fixture
{
    private static $names = ['To do', 'In progress', 'Done'];

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < count(self::$names); $i++) {
            $stage = new TaskStage();
            $stage->setName(self::$names[$i]);
            $priority = $i + 1;
            $stage->setPriority($priority);
            $stage->setIsClosingStage($i === array_key_last(self::$names));

            $manager->persist($stage);
        }
        $manager->flush();
    }
}
